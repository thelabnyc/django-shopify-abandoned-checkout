# Generated by Django 2.2.4 on 2019-11-08 17:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shopify_abandoned_checkout', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='abandonedcheckout',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='abandonedcheckout',
            name='modified_at',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
