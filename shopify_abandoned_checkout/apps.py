from django.apps import AppConfig


class ShopifyAbandonedCheckoutConfig(AppConfig):
    name = 'shopify_abandoned_checkout'
